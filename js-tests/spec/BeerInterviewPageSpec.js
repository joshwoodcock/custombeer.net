define(['jquery','models/Account','models/DescriptionList','views/ScalePanel','models/BeerInterview','views/BeerList','jasmine-jquery','util'], 
function($, Account, DescriptionList, ScalePanel, BeerInterview, BeerList, jasquery, util){
	

	return describe('On the beer rate page', function () {
		
		var account;
		var scalePanel;
		var descriptionList;
		var beerInterview;
		
	    beforeEach(function () {
	    	loadFixtures('rate.html');
	    	
	    	account = new Account();
	    	
	    	descriptionList = new DescriptionList({page:'rate-a-beer', get_beer_list:true});
	    	var beerList            = new BeerList({el:'#beer-list', model:descriptionList});
	    	beerInterview   = new BeerInterview({account:account, description_list:descriptionList, beer_list:beerList});
			
	    	var scalePanel  = new ScalePanel({el:'#sliders', model: descriptionList});

	    	account.logoutSubmit();

	    	removeAllCookies();
	    });
	    
	    afterEach(function(){
	    	$("#jasmine-fixtures").html("");	
	    });
	    
		it("the users previous rates are loaded when they log in", function(){
			
			// Wait for description list to finish loading and the account to log out
			waitsFor(function(){
				var is_logged_in = account.isLoggedIn();
				if(descriptionList.get('setup_complete') == true && is_logged_in == false)
				{
					return true;
				}else{
					return false;
				}
			});
			
			// Check whether or not the rates have been loaded into to view.
			runs(function(){
				
				// Get the view value of the first attribute value
				var attribute_value = parseInt($('#descriptor_scale_1').html());
				
				// The first beer that is loaded into the UI should be 
				expect(attribute_value).toBe(3);
			});
			
			// Log the test user in.
			runs(function(){
				
				var username = 'test@me.com';
				var password = 'queryquery67^&';
				
				account.loginSubmit(username, password);
			});
			
			// Wait for the user to be logged in
			waitsFor(function(){
				return account.isLoggedIn();
			},"The user's rates should be loaded after they log in",5000);
			
			// Wait for the setup to re-complete
			waitsFor(function(){
				return (beerInterview.get('refreshing_beer') == false);
			},"The server must requery the beer list to get the user's rates",5000);
			
			// Check whether or not the rates have been loaded into to view.
			runs(function(){
				
				// Get the view value of the first attribute value
				var attribute_value = parseInt($('#descriptor_scale_1').html());
				
				// The first beer that is loaded into the UI after a user has logged in should be 
				expect(attribute_value).toBe(5);
			});
		});
		
	    describe('when model is validating', function () {
	        var errors;
	    });
	});
	
});