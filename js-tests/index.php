<!DOCTYPE html>
<html>
<head>
  <?php
  	$config=array();
  	require_once '../application/config/site.php';
  ?>
  <title>Jasmine Spec Runner</title>
  <link rel="stylesheet" type="text/css" href="./lib/jasmine/jasmine.css">
  <link rel="shortcut icon" href="./lib/jasmine/jasmine_favicon.png" type="image/x-icon" />
  <script>
  var site='<?php echo $config['site']?>';

  // Sets cache busting equal to true. This is good for debugging but bad for a production environment.
  var cb=false;

  </script>
  <script src="../js/application/libraries/require-1.1.min.js"></script>
  <script src="../js/application/deps.js" ></script>
  <script src="./spec/AllSpecs.js"></script>
</head>
<body>
  <div id="sandbox" style="overflow: hidden; height: 1px;"></div>
</body>
</html>