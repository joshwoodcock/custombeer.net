<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class order extends CI_Controller {

	public function submit()
	{
		session_start();

		$user_id = 0;
		$country = 'us';
		$id      = 0;
		$zip     = 0;
		$email   = '';


		/*start Remove requirement to be logged in to make an order
		if(isset($_SESSION['user_id']) == false)
		{
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(array('result'=>'failure','message'=>'You must be logged in to place an order.'));
			return;
		}else{
			$user_id = $_SESSION['user_id'];
		}
		End remove requirement to be logged in to make an order*/

		$this->load->database();

		$data = null;
		if(isset($_POST['description']))
		{
			$data = $_POST['description'];
		}else{
			show_error("argument not set: 'description'",400);
		}

		if(isset($_POST['country']))
		{
			$country = mysql_real_escape_string($_POST['country'], $this->db->conn_id);
		}else{
			show_error("argument not set: 'country'",400);
		}

		if(isset($_POST['zip']))
		{
			$zip = mysql_real_escape_string($_POST['zip'], $this->db->conn_id);
		}else{
			$result = array('result'=>'failure','message'=>'Zip code is not valid.');
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return;
		}

		if(isset($_POST['email']))
		{
			$email = mysql_real_escape_string($_POST['email'], $this->db->conn_id);
		}else{
			$result = array('result'=>'failure','message'=>'Email is not valid.');
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return;
		}

		$description = json_decode($data);
		$db = $this->config->item('default');

		$con=mysqli_connect($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);

		$q = "
		INSERT INTO t_order(`user_id`,`country`,`zip_code`,`email`) VALUES({$user_id},'{$country}','{$zip}','{$email}');
		SELECT LAST_INSERT_ID() AS `id`;";

		$description_id = '';
		if(mysqli_multi_query($con, $q))
		{
			while(mysqli_next_result($con))
			{
				if($result=mysqli_store_result($con))
				{
					while($row=mysqli_fetch_row($result)){
						$description_id=$row[0];
					}
					mysqli_free_result($result);
				}
			}
		}

		$q = "INSERT INTO `t_order_values`(`order_id`,`descriptor_id`,`value`)
		VALUES";
		$values = array();
		foreach($description->a as $attr)
		{
			$id  = $attr->i;
			$val = $attr->v;
			array_push($values,"({$description_id},{$id},{$val})");
		}
		$q .= implode(',',$values);

		$this->db->query($q);

		$result = array('result'=>'success','desc_id'=>$description_id, 'message'=>'Your order has been submitted');

		$email = new EmailSender();
		$this->load->database();
		$this->db->select('email');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get('t_user');

		$user_email = '';
		if($query->num_rows == 1)
		{
			$user_email = $query->row()->email;
		}

		$success = $email->emailContent('joshwoodcock@beagile.biz', $user_email.' has ordered a custom beer!', $user_email.' has ordered a custom beer!');
		if($success == false)
		{
			throw new Exception('Email notice failed');
		}

		header('Content-Type: application/json; charset=utf-8');

		echo json_encode($result);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */