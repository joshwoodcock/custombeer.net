<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class account extends CI_Controller {

	private function checkLang($lang)
	{
		$this->load->database();

		$lang = mysql_real_escape_string($lang);

		$query = $this->db->query("SELECT `lang` FROM `t_content_lang` WHERE `lang` = '{$lang}'");

		if($query->num_rows() == 0)
		{
			show_error("Invalid language provided at: lang='{$lang}'",400);
		}
	}

	public function signup()
	{
		session_start();

		$lang     = 'en';
		$country  = 'us';
		$email    = '';
		$password = '';

		$this->load->database();

		if(isset($_POST['lang']) && $_POST['lang'] != ''){$lang = $_POST['lang'];}

		$this->checkLang($lang);

		// Email.
		if(isset($_POST['email']) && $_POST['email'] != ''){
			$email = mysql_real_escape_string($_POST['email']);
		}else{
			show_error("Invalid email provided at: email='{$email}'",400);
		}

		// Password.
		if(isset($_POST['password']) && $_POST['password'] != '')
		{
			$password = mysql_real_escape_string($_POST['password']);
		}else{
			show_error("Invalid password provided at: password=''",400);
		}

		// Check if email already exists.
		$query = $this->db->query("SELECT `email` FROM `t_user` WHERE `email` = '{$email}'");
		if($query->num_rows() > 0)
		{
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(array('result'=>'failure','message'=>'The email provided already exists.'));
			return;
		}

		// DO NOT CHANGE!!!!
		$encrypted_pass = sha1(sha1($password).sha1(SALT));
		// DO NOT CHANGE!!!!

		$q = "
			INSERT INTO t_user(`email`,`password`,`signup_date`,`last_page_load`,`country`)
			VALUES('{$email}','{$encrypted_pass}',NOW(),NOW(),'{$country}');
			SELECT LAST_INSERT_ID() AS `id`;";

		$user_id = '';
		$db = $this->config->item('default');
		$con=mysqli_connect($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);
		if(mysqli_multi_query($con, $q))
		{
			while(mysqli_next_result($con))
			{
				if($result=mysqli_store_result($con))
				{
					while($row=mysqli_fetch_row($result)){
						$user_id=$row[0];
					}
					mysqli_free_result($result);
				}
			}
		}

		$response = array('result'=>'success','user_id'=>$user_id);

		$_SESSION['user_id'] = $user_id;

		header('Content-Type: application/json; charset=utf-8');

		echo json_encode($response);
	}

	public function login()
	{
		session_start();

		$lang     = 'en';
		$country  = 'us';
		$email    = '';
		$password = '';

		$data = json_decode(file_get_contents('php://input'));

		$this->load->database();

		if(isset($_POST['lang']) && $_POST['lang'] != ''){$lang = $_POST['lang'];}

		$this->checkLang($lang);

		// Email.
		if(isset($data->email) && $data->email != ''){
			$email = mysql_real_escape_string($data->email);
		}else{
			show_error("Invalid email provided at: email='{$email}'",400);
		}

		// Password.
		if(isset($data->password) && $data->password != '')
		{
			$password = mysql_real_escape_string($data->password);
		}else{
			show_error("Invalid password provided at: password=''",400);
		}

		$encrypted_pass = $this->encrypt_password($password);

		// Check if email already exists.
		$query = $this->db->query("SELECT `user_id`,`password` FROM `t_user` WHERE `email` = '{$email}'");
		$user_id = null;
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$row = $result[0];

			$stored_encrypted_password = $row->password;

			if($encrypted_pass != $stored_encrypted_password)
			{
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode(array('success'=>false,'message'=>'The password for the email provided is incorrect.','name'=>'password'));
				return;
			}else{
				$user_id = $row->user_id;
			}
		}else{
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode(array('success'=>false,'message'=>'The email provided does not exist.','name'=>'email'));
			return;
		}

		$response = array('success'=>true,'user_id'=>$user_id);

		$_SESSION['user_id'] = $user_id;

		header('Content-Type: application/json; charset=utf-8');

		echo json_encode($response);
	}

	public function logout()
	{
		session_start();

		session_destroy();

		header('Content-Type: application/json; charset=utf-8');

		echo json_encode(array('result'=>'success'));
	}

	public function forgot_password(){
		$lang     = 'en';
		$country  = 'us';
		$email    = '';
		$password = '';

		$this->load->database();

		if(isset($_POST['lang']) && $_POST['lang'] != ''){$lang = $_POST['lang'];}

		$this->checkLang($lang);

		// Email.
		if(isset($_POST['email']) && $_POST['email'] != ''){
			$email = mysql_real_escape_string($_POST['email']);
		}else{
			show_error("Invalid email provided at: email='{$email}'",400);
		}

		$query = $this->db->query("SELECT `user_id` FROM `t_user` WHERE email='{$email}'");

		if($query->num_rows() <= 0)
		{
			header('Content-Type: application/json; charset=utf-8');

			echo json_encode(array('result'=>'failure', 'message'=>'The email provided does not exist.'));

			return;
		}

		$data = array();

		$_GET['email'] = $email;

		$reset_id = sha1(time() . SALT . $email);

		$reset_id_hash = sha1($reset_id . SALT2);

		$query = $this->db->query("UPDATE `t_user` SET `reset_id_hash` = '{$reset_id_hash}', `password_reset_time` = NOW() WHERE email='{$email}'");

		$_GET['reset_id'] = $reset_id;
		$_GET['show_web'] = 'true';

		ob_start();

		//$this->load->view('email/password-reset/plain-text');
		$this->load->view('email/password-reset/html');

		$lstrOutput = ob_get_clean();

		$lobjEmail = new EmailSender();
		$lboolSuccess = $lobjEmail->emailContent($email, 'Password Reset Link for CustomBeer.net', $lstrOutput);


		header('Content-Type: application/json; charset=utf-8');

		if($lboolSuccess == true)
		{
			echo json_encode(array('result'=>'success', 'message'=>'A link has been sent to the email you have provided.'));
		}else{
			echo json_encode(array('result'=>'failure', 'message'=>'An error has occured.','error_type'=>'unknown'));
		}


	}

	public function form_forgot_password()
	{
		$email = '';
		$reset_id = '';
		$data = array();
		$password = '';
		$password_confirm = '';

		$this->load->database();
		$data['invalid'] = false;
		$data['site'] = $this->config->item('site');
		$data['error'] = '';
		$data['success'] = '';

		// Email.
		if(isset($_GET['email']) && $_GET['email'] != ''){
			$email = mysql_real_escape_string($_GET['email']);
		}else{
			show_error("Invalid email provided at: email='{$email}'",400);
		}

		// Validate Email
		$query = $this->db->query("SELECT `user_id` FROM `t_user` WHERE email='{$email}'");
		if($query->num_rows() <= 0)
		{
			$data['invalid'] = true;
			$data['error'] = 'The email provided does not exist';
			$this->load->view('forgot-password',$data);
			return;
		}

		// Reset ID.
		if(isset($_GET['reset_id']) && $_GET['reset_id'] != ''){
			$reset_id = mysql_real_escape_string($_GET['reset_id']);
		}else{
			show_error("Invalid reset id provided at: reset_id='{$reset_id}'",400);
		}

		// Validate Reset ID
		$reset_id_hash = sha1($reset_id . SALT2);
		$query = $this->db->query("
				SELECT `user_id`
				FROM `t_user`
				WHERE `reset_id_hash`='{$reset_id_hash}'
				AND email='{$email}'
				AND `password_reset_time` > DATE_SUB( NOW(), INTERVAL 24 HOUR)");

		if($query->num_rows() <= 0)
		{
			$data['invalid'] = true;
			$data['error'] = 'The reset id provided is invalid';
			$this->load->view('forgot-password',$data);
			return;
		}

		// Password
		if(isset($_POST['password'])){
			$password = mysql_real_escape_string($_POST['password']);
		}else{
			$this->load->view('forgot-password',$data);
			return;
		}

		// Password Confirm
		if(isset($_POST['password_confirm'])){
			$password_confirm = mysql_real_escape_string($_POST['password_confirm']);
		}else{
			$this->load->view('forgot-password',$data);
			return;
		}

		if(strlen($password) < 8)
		{
			$data['error'] = 'Password must be at least 8 characters';
			$this->load->view('forgot-password',$data);
			return;
		}

		if($password !== $password_confirm)
		{
			$data['error'] = 'Passwords do not match';
			$this->load->view('forgot-password',$data);
			return;
		}

		$password_hash = $this->encrypt_password($password);
		$query = $this->db->query("UPDATE `t_user` SET `password` = '{$password_hash}', `password_reset_time` = '0000-00-00 00:00:00', `reset_id_hash`='' WHERE email='{$email}'");


		$data['invalid'] = true;
		$data['success'] = 'Your password has been changed';
		$this->load->view('forgot-password',$data);
	}

	/**
	 * DO NOT CHANGE!!
	 *
	 * @param password $password
	 *
	 * @return string
	 */
	private function encrypt_password($password)
	{
		// DO NOT CHANGE!!!!
		$encrypted_pass = sha1(sha1($password).sha1(SALT));
		// DO NOT CHANGE!!!!

		return $encrypted_pass;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */