<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class email extends CI_Controller {

	public function preview($campaign)
	{
		$this->load->view("email/{$campaign}/html");
	}

	public function feedback()
	{
		$body = $_POST['message'];
		$email = new EmailSender();

		$email->emailContent('joshwoodcock@beagile.biz', 'New feedback for CustomBeer.net', $body);

		header("Location: " . $_SERVER['HTTP_REFERER']);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */