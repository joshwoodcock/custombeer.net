<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class order_page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		session_start();

		$data    = array();
		$lang    = 'en';
		$country = 'us';
		$version = 1;

		if(isset($_GET['lang']) && $_GET['lang'] != ''){$lang = $_GET['lang'];}

		$this->checkLang($lang);

		header('Accept-Encoding: gzip, deflate');

		$query = $this->db->query("
				SELECT `field_number`, `value`
				FROM  t_content_field_". $lang);

		foreach($query->result() as $row)
		{
			$data['lang'][$row->field_number] = $row->value;
		}

		if(isset($_GET['version']) && $_GET['version'] != ''){$version = $_GET['version'];}

		$data['logo']       = 'beer.png';
		$data['cta']        = 'order-cta.php';
		$data['page']       = 'front-page';
		$data['title']      = $data['lang']['2'];
		$data['tagline']    = $data['lang']['3'];
		$data['site']       = $this->config->item('site');
		$this->load->view('main', $data);

	}

	private function checkLang($lang)
	{
		$this->load->database();

		$lang = mysql_real_escape_string($lang);

		$query = $this->db->query("SELECT `lang` FROM `t_content_lang` WHERE `lang` = '{$lang}'");

		if($query->num_rows() == 0)
		{
			show_error("Invalid language provided at: lang='{$lang}'",400);
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */