<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rate_a_beer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		session_start();

		$data    = array();
		$lang    = 'en';
		$country = 'us';
		$level   = 1;

		if(isset($_GET['lang']) && $_GET['lang'] != ''){$lang = $_GET['lang'];}

		$this->checkLang($lang);

		header('Accept-Encoding: gzip, deflate');

		$query = $this->db->query("
				SELECT `field_number`, `value`
				FROM  t_content_field_". $lang);

		foreach($query->result() as $row)
		{
			$data['lang'][$row->field_number] = $row->value;
		}

		$query = $this->db->query("
				SELECT `img_url`
				FROM t_descriptor_beer
				WHERE `level` = {$level}");

		$data['beer_images'] = array();
		foreach($query->result() as $row)
		{
			array_push($data['beer_images'],$row->img_url);
		}

		$data['logo']       = 'ajax-loader-large.gif';
		$data['cta']        = 'rate-cta.php';
		$data['page']       = 'rate-a-beer';
		$data['title']      = $data['lang']['38'];
		$data['tagline']    = $data['lang']['39'];
		$data['site']       = $this->config->item('site');

		$this->load->view('main', $data);

	}

	public function get_beer_list()
	{
		$beers = array();
		$level = 1;
		$country = 'us';
		$user_id = 0;

		if(isset($_GET['level']) && $_GET['level'] != ''){$level = $_GET['level'];}

		session_start();

		if(isset($_SESSION['user_id']))
		{
			$user_id = $_SESSION['user_id'];
		}

		$this->load->database();


		$query = $this->db->query("
			SELECT `beer_id`,`beer_name`,`img_url`
			FROM  t_descriptor_beer
			WHERE level = {$level}
			ORDER BY `beer_name` ASC");

		$beers = array();

		foreach($query->result() as $row)
		{
			$description = array();
			$attr_vals   = array();
			$submitted   = false;
			$site        = $this->config->item('site');

			if($user_id != 0)
			{
				$beer_id = $row->beer_id;

				$q = "
				SELECT `description_instance_id`,`t_description_values`.`descriptor_id`,`t_description_values`.`value`
				FROM `t_description`
				INNER JOIN `t_description_values` ON `t_description`.`instance_id` = `t_description_values`.`description_instance_id`
				INNER JOIN `t_descriptor_{$country}` ON `t_descriptor_{$country}`.`descriptor_id` = `t_description_values`.`descriptor_id`
				WHERE `t_description`.`user_id` = {$user_id} AND `beer_id` = {$beer_id}
				GROUP BY `t_description`.`beer_id`, `t_description_values`.`descriptor_id`
				ORDER BY `t_description`.`instance_id` DESC, `t_descriptor_{$country}`.`index` ASC";

				$sub_query = $this->db->query($q);

				if($sub_query->num_rows() > 0)
				{
					$attr_vals = array();
					$submitted = true;
					foreach($sub_query->result() as $sub_row)
					{
						array_push($attr_vals, array('di'=>$sub_row->description_instance_id,'id'=>$sub_row->descriptor_id,'val'=>$sub_row->value));
					}
				}
			}

			if(count($attr_vals) == 0)
			{
				// Default attribute values. TODO.
				$q = "
						SELECT `descriptor_id`, `initial_value`
						FROM t_descriptor_{$country}
						ORDER BY `index` ASC";

				$sub_query = $this->db->query($q);

				foreach($sub_query->result() as $sub_row)
				{
					array_push($attr_vals, array('id'=>$sub_row->descriptor_id,'val'=>$sub_row->initial_value));
				}
			}

			array_push($beers,array(
				'submitted'=>$submitted,
				'id'=>$row->beer_id,
				'name'=>$row->beer_name,
				'img'=> $row->img_url,
				'attribute_values'=>$attr_vals
			));
		}


		header('Content-Type: application/json; charset=utf-8');

		echo json_encode($beers);
	}

	public function save_rate()
	{
		$country = 'us';

		$user_id = 0;

		session_start();

		if(isset($_SESSION['user_id']))
		{
			$user_id = $_SESSION['user_id'];
		}

		$this->load->database();

		$data = null;
		if(isset($_POST['description']))
		{
			$data = $_POST['description'];
		}else{
			show_error("argument not set: 'description'",400);
		}

		if(isset($_POST['country']))
		{
			$country = mysql_real_escape_string($_POST['country'], $this->db->conn_id);
		}else{
			show_error("argument not set: 'country'",400);
		}

		$description = json_decode($data);

		if(isset($description->di) && $description->di != null)
		{
			$this->update_rate($user_id, $description->di);

			$description_id =  $description->di;
		}else{
			$description_id = $this->create_rate($user_id, $description, $country);
		}

		$result = array('result'=>'success','desc_id'=>$description_id);

		header('Content-Type: application/json; charset=utf-8');

		echo json_encode($result);
	}

	private function create_rate($user_id, $description, $country)
	{
		$id      = 0;
		$beer_id = $description->id;
		$db      = $this->config->item('default');

		$con=mysqli_connect($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);

		$q = "
		INSERT INTO t_description(`beer_id`,`user_id`,`country`) VALUES({$beer_id},{$user_id},'{$country}');
		SELECT LAST_INSERT_ID() AS `id`;";

		$description_id = '';
		if(mysqli_multi_query($con, $q))
		{
			while(mysqli_next_result($con))
			{
				if($result=mysqli_store_result($con))
				{
					while($row=mysqli_fetch_row($result)){
						$description_id=$row[0];
					}
					mysqli_free_result($result);
				}
			}
		}

		$q = "INSERT INTO `t_description_values`(`description_instance_id`,`descriptor_id`,`value`)
		VALUES";
		$values = array();

		foreach($description->a as $attr)
		{
			$id  = $attr->i;
			$val = $attr->v;
			array_push($values,"({$description_id},{$id},{$val})");
		}

		$q .= implode(',',$values);

		$this->db->query($q);

		return  $description_id;
	}

	private function update_rate($user_id, $desc_id)
	{
		$q = "UPDATE `t_description` SET `user_id` = {$user_id} WHERE `instance_id` = {$desc_id} AND `user_id` = 0;";

		$this->db->query($q);
	}

	private function checkLang($lang)
	{
		$this->load->database();

		$lang = mysql_real_escape_string($lang);

		$query = $this->db->query("SELECT `lang` FROM `t_content_lang` WHERE `lang` = '{$lang}'");

		if($query->num_rows() == 0)
		{
			show_error("Invalid language provided at: lang='{$lang}'",400);
		}
	}

}