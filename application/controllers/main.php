<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class main extends CI_Controller {

	public function beer_description()
	{
		$lang    = 'en';
		$country = 'us';

		if(isset($_GET['lang']) && $_GET['lang'] != ''){$lang = $_GET['lang'];}

		$this->checkLang($lang);

		header('Content-Type: application/json; charset=utf-8');
		header("Content-Language: {$lang}");

		$this->load->model('attribute_list');

		$attribute_list = new attribute_list();

		$attribute_list->setup($country, $lang);

		echo json_encode($attribute_list);
	}

	private function checkLang($lang)
	{
		$this->load->database();

		$lang = mysql_real_escape_string($lang);

		$query = $this->db->query("SELECT `lang` FROM `t_content_lang` WHERE `lang` = '{$lang}'");

		if($query->num_rows() == 0)
		{
			show_error("Invalid language provided at: lang='{$lang}'",400);
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */