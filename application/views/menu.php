
<div class="menu" id="menu">
	<ul>
		<?php if($page!='rate-a-beer'){
			?><li><a href="<?php echo $site;?>/rate_a_beer"><img alt="beer variety" src="<?php echo $site;?>/img/beer-variety.png"><span>Interview a Beer</span></a></li><?php
		}?>
		<?php if($page!='front-page'){
			?><li><a href="<?php echo $site;?>/"><img alt="beer mug" src="<?php  echo $site;?>/img/beer-small.png"><span>Make a Beer</span></a></li><?php
		}?>
		<?php if($page!='view-orders'&&false==true){
			?><li><a href="<?php echo $site;?>/"><img alt="beer barrel" src="<?php echo $site;?>/img/barrel.png"><span>View My Orders</span></a></li><?php
		}?>
		<?php if($page!='find-beer-type'&&false==true){
			?><li><a href="<?php echo $site;?>/"><img alt="beer type" src="<?php echo $site;?>/img/free-beer.png"><span>Find Beer Type</span></a></li><?php
		}?>


		<li id="beer-points">
		 	<a class="render" href="#beer-points">
		 		<img alt="free beer" src="<?php echo $site;?>/img/free-beer.png">
		 		<span>Earn Free Beer Points</span>
		 	</a>

		</li>

		<li id="more"><a href="#more"><img alt="beer half full" src="<?php echo $site;?>/img/half-full-beer.png"><span>More</span></a></li>
	</ul>

	<?php include "beer-points.php";?>
	<?php include "more-content.php";?>
	<?php include "signup-content.php";?>
	<?php include "login-content.php";?>
	<?php include "forgot-password-content.php"?>
</div>



<!-- preload the images -->
<div class="invisible">
	<img alt="close modal" src='<?php echo $site;?>/img/basic/x.png' alt='' />
</div>

