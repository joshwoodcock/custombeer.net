<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $title; ?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
		<meta name="copyright" content="Custombeer.net is a copyright of Be Agile, LLC"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="description" content="Make custom beer. What is your style? color, flavor, bitterness, density, alcohol by volume - order my custom beer"/>
		<meta name="keywords" content="custom beer, free beer, personalized beer, beer recipe, custom brew, color, flavor, bitterness, density, alcohol by volume"/>
		<meta name="robots" content="index,follow"/>
		<meta name="DC.title" content="<?php echo $title; ?>"/>


		<link href="<?php echo $site;?>/css/jquery.nouislider.css" rel="stylesheet"/>
		<link href="<?php echo $site;?>/css/jquery.qtip-2.2.0.min.css" rel="stylesheet"/>
		<link rel="shortcut icon" href="<?php echo $site;?>/img/beer.ico" type="image/x-icon" />
		<link href="<?php echo $site;?>/css/main.css" rel="stylesheet"/>
		<link href="<?php echo $site;?>/css/<?php echo $page; ?>.css" rel="stylesheet"/>
	</head>
	<body>
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->


		<div id="bg">
			<div class="container">
				<h1 id="title-text"><?php echo $title; ?></h1>
				<?php include $page.'/over-beer.php';?>
				<div class="logo-container">
					<img alt="site logo - beer mug" class="logo" src="<?php echo $site;?>/img/<?php echo $logo; ?>">
					<img alt="wide down arrow" class="arrow-down-img-horizontal" src="<?php echo $site;?>/img/down-arrow.png">
				</div>

				<?php include $page.'/under-beer.php';?>
				<span class="title-text2" ><?php echo $tagline; ?></span>
				<span class="img-loader"><img alt="ajax loader" src="<?php echo $site;?>/img/ajax-loader.gif"/></span>
				<div id="sliders">
					<div style="display:none">
						<div class="tip-container"></div>
						<h2 class="sliderName"><img alt="universal info sign" src="<?php echo $site;?>/img/question.png" class="info-img"></h2>
						<span id="descriptor_scale_1" class="descriptor-value">3</span>
						<div id="descriptor_id_0" class="noUiSlider"></div>
						<span class="sliderText">
							<span class="sliderEndText sliderLow" id="low-descriptor0">Light</span>
							<span class="sliderEndText sliderHigh" id="high-descriptor0">Dark</span>
						</span>
					</div>
				</div>
				<?php include $cta; ?>
				<?php include 'menu.php'; ?>
				<?php include $page . '/content.php';?>

				<!-- Start Temp Feedback Form -->
				<p>Have ideas about how to improve this app? Have questions? Let us know.</p>
				<form id="feedback-form" class="feedback-form" action="email/feedback" method="post" style="display:inline-block">
					<span><textarea name="message" style="width:95%;height:100px"></textarea></span>
					<a href="#" onclick="feedback()" class="btn" style="margin-bottom:10px">Send Feedback</a>
				</form>
				<script>
				function feedback()
				{
					$("#feedback-form").submit();
					alert('Thanks for your feedback!');
				}
				</script>

				<!-- End Temp Feedback Form -->

			</div>
			<input type="hidden" value="false" id="logged-in"/>


		</div>
		<script>var site='<?php echo $site;?>';</script>
		<script src="<?php echo $site;?>/js/application/libraries/require-1.1.min.js"></script>
		<script src="<?php echo $site;?>/js/application/deps.js"></script>
		<!-- <script src="<?php echo $site;?>/js/application/main.js"></script> -->
		<script src="<?php echo $site;?>/js/<?php echo $page;?>.js"></script>

	</body>
</html>
