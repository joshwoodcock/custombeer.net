
						<!-- modal content -->
			<div id="order-content" class="invisible signup-content modal-content">
				<div>
				<p>
					<p class="close-modal">
						<a href="#close" class="simplemodal-close">[x]close</a>
					</p>
				</p>
					<h3>Confirm Order</h3>
					When you submit your order this is what will happen:
					<ol>
						<li>We store your order</li>
						<li>We find a brew master in your area to make your custom beer (0-6 weeks)</li>
						<li>We send you a link by email to request payment and age validation. Price varies based on location and local laws.</li>
						<li>You pay for your custom beer</li>
						<li>Your brew master crafts your custom beer (3-4 weeks)</li>
						<li>The brew master ships your custom beer directly to your front door</li>
						<li>Enjoy your uniquely crafted and bottled custom beer</li>
					</ol>
					<div class="form-area">
						<div class="form-boxes">
							<label for="order-zip">*Zip Code</label>
							<input id="order-zip" value="" type="number"/>
							<label for="order-email">*Email</label>
							<input id="order-email" value="" type="text"/>
							<a href="#submit-order" class="form-box-button submit-order">Submit Order</a>
						</div>
					</div>
				</div>
				<div id="order-message" class="error"></div>
				<p>
					<p class="close-modal">
						<a href="#close" class="simplemodal-close">[x]close</a>
					</p>
				</p>

			</div>