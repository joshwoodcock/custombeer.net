

			<!-- modal content -->
			<div id="login-content" class="invisible login-content modal-content">
				<div>
					<h3>Login</h3>

					<div class="form-area">
						<div class="form-boxes">
							<label for="login-email">*Email</label>
							<input id="login-email" value="" type="email"/>

							<label for="login-password">*Password</label>
							<input id="login-password" value="" type="password"/>

							<a href="#login" class="form-box-button login-button">Log In</a>
							<a href="#forgot-password" class="forgot-password">Forgot Password</a>
						</div>
					</div>
					<p id="login-error" class="error"></p>
				</div>
				<p>
					<a class="signup" href='#signup'>Sign Up</a>
					<p class="close-modal">
						<a href="#close" class="simplemodal-close">[x]close</a>
					</p>
				</p>

			</div>