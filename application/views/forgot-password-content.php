

			<!-- modal content -->
			<div id="forgot-password-content" class="invisible modal-content">
				<div>
					<h3>Forgot Password</h3>

					<div class="form-area">
						<div class="form-boxes">
							<label for="forgot-password-email">*Email</label>
							<input id="forgot-password-email" value="" type="email"/>

							<a href="#send-reset-link" class="form-box-button forgot-password-submit">Send Reset Link</a>
						</div>
					</div>
					<p  id="forgot-password-error" class="error"><span class="loader"></span></p>
				</div>
				<p>
					<a class="login" href='#login'>Log In</a>
					<p class="close-modal">
						<a href="#close" class="simplemodal-close">[x]close</a>
					</p>
				</p>

			</div>