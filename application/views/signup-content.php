
						<!-- modal content -->
			<div id="signup-content" class="invisible signup-content modal-content">
				<div>
					<h3>Sign Up</h3>

					<div class="form-area">
						<div class="form-boxes">
							<label for="signup-email">*Email</label>
							<input id="signup-email" value="" type="email"/>

							<label for="signup-password">*Password</label>
							<input id="signup-password" value="" type="password"/>

							<label for="signup-password-confirm">*Confirm Password</label>
							<input id="signup-password-confirm" value="" type="password"/>

							<a href="#signup" class="form-box-button signup-button">Sign Up</a>
						</div>
					</div>
					<p id="signup-error" class="error"></p>
				</div>
				<p >
					<a class="login" href='#login'>Log In</a>
					<p class="close-modal">
						<a href="#close" class="simplemodal-close">[x]close</a>
					</p>
				</p>

			</div>
