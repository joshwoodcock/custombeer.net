<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Change Forgotten Password</title>
		<link rel="shortcut icon" href="<?php echo $site;?>/img/beer.ico" type="image/x-icon" />
		<link href="<?php echo $site;?>/css/main.css" rel="stylesheet">
		<link href="<?php echo $site;?>/css/forgot-password.css" rel="stylesheet">
	</head>
	<body>
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->


		<div id="bg">
			<div class="container">
				<h3>Change Password</h3>


				<div class="form-area">
					<div class="form-boxes">
					<?php
					if($invalid == false)
					{
						?>
						<form action="" method="post">
							<label for="password">*New Password</label>
							<input id="password" type="password" name="password" value=""/>

							<label for="password_confirm">*Confirm New Password</label>
							<input id="password_confirm" type="password" name="password_confirm" value=""/>

							<span class="submit"><input class="form-box-button" type="submit" value="Change"/></span>
						</form>
						<br/><br/>

						<?php
					}
					?>

						<?php
						if($error != '')
						{
							?><div class="error"><?php echo $error;?></div><?php
						}elseif($success != ''){
							?><div class="success"><?php echo $success;?></div>
							<a href="<?php echo $site;?>">Return to Main Page</a><?php
						}
						?>
					</div>


				</div>


			</div>
		</div>

	</body>
</html>
