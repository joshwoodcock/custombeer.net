
			<!-- modal content -->
			<div id="beer-points-content" class="invisible beer-points-content modal-content">
				<h3>Earn Free Beer Points</h3>
				<p>
				<?php
				if(isset($_SESSION['user_id']))
				{
					?>Each<?php
				}else{
					?>If you are signed in, each<?php
				}
				?> time you submit a beer rating, you will earn free beer points. Beer points can be used to purchase part of your custom beer order.</p>

				<p>
					<a class="signup" href='#signup'>Sign Up</a>
					<span class="signup-login" >&nbsp;or&nbsp;</span>
					<a class="login" href='#login'>Log In</a>
				</p>
				<p class="close-modal">
						<a href="#close" class="simplemodal-close">[x]close</a>
				</p>
			</div>
