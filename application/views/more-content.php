			<!-- modal content -->
			<div id="more-content" class="invisible modal-content">
				<div class="menu">

					<ul>
						<li class="login"><a class="login" href='#login'><img alt="enter beer saloon" src="<?php echo $site;?>/img/enter-saloon.png"><span>Log In</span></a></li>
						<li class="signup"><a class="signup" href='#signup'><img alt="sign up for custom beer" src="<?php echo $site;?>/img/free-beer.png"><span>Sign Up</span></a></li>
						<li class="logout invisible"><a class="logout" href='#logout'><img alt="log out of custom beer" src="<?php echo $site;?>/img/leave-saloon.png"><span>Log Out</span></a></li>
						<li><a href='mailto:feedback@<?php echo $_SERVER['HTTP_HOST'];?>'><img alt="contact custom beer email" src="<?php echo $site;?>/img/email.png"></a><span>feedback@<?php echo $_SERVER['HTTP_HOST'];?></span></li>
					</ul>

				</div>
				<div id="more-error" class="error"></div>
					<p class="close-modal">
							<a alt="modal close" href="#close" class="simplemodal-close">[x]close</a>
					</p>

			</div>