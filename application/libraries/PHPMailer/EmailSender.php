<?php
/**
 * A class that sends emails.
 *
 * PHP Version 5.4
 *
 * @category PHP
 * @package StockLogger
 * @subpackage EmailSender
 * @author Josh Woodcock <joshwoodcock@beagile.biz>
 * @copyright 2012-2013 Be Agile, LLC
 * @license http://beagile.biz Contractual License Program
 * @link http://beagile.biz
 */

require_once 'class.phpmailer.php';

/**
 * EmailSender.
 */
class EmailSender
{

	/**
	 * The default from address for this application.
	 *
	 * @var string
	 */
	private $mstrFrom = 'custombeer@beagile.biz';

	/**
	 * The password to use for smtp mailing.
	 *
	 * @var string
	 */
	private $mstrPassword = 'Q]}tb7@)+s]a';

	private $mstrMessage = '';

	/**
	 * Sends an email.
	 *
	 * @param string $lstrTo      The email to send to.
	 * @param string $lstrSubject The subject of the mail.
	 * @param string $lstrContent The content of the body.
	 *
	 * @return void
	 */
	public function emailContent($lstrTo, $lstrSubject, $lstrContent, $lstrContentType='text/html')
	{
		$lobjMail = new PHPMailer();

		$lobjMail->isSMTP();
		$lobjMail->CharSet     = 'UTF-8';
		$lobjMail->Host        = 'mail.beagile.biz';
		$lobjMail->SMTPDebug   = 0;
		$lobjMail->SMTPAuth    = true;
		$lobjMail->Port        = 587;
		$lobjMail->Username    = $this->mstrFrom;
		$lobjMail->Password    = $this->mstrPassword;
		$lobjMail->ContentType = $lstrContentType;
		$lobjMail->Subject     = $lstrSubject;
		$lobjMail->Body        = $lstrContent;
		$lobjMail->SMTPSecure  = 'ssl/tls';
		$lobjMail->From        = $this->mstrFrom;
		$lobjMail->FromName    = 'CustomBeer.net';

		$lobjMail->addAddress($lstrTo);

		$lboolSuccess = $lobjMail->send();

		$this->mstrMessage = $lobjMail->ErrorInfo;

		return $lboolSuccess;
	}

	public function setFrom($lstrEmail)
	{
		$this->mstrFrom = $lstrEmail;
	}

}
?>