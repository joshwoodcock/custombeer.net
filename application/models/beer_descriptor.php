<?php
/**
 * .
 *
 * PHP Version 5.4
 *
 * @category PHP
 * @package
 * @subpackage None
 * @author Josh
 * @copyright 2012-2014 Be Agile, LLC
 * @license http://beagile.biz Contractual License Program
 * @link http://beagile.biz
 */

class beer_descriptor
{
	/**
	 * A unique id of the descriptor.
	 *
	 * @var integer
	 */
	public $id;

	/**
	 * The name of the descriptor.
	 *
	 * @var string
	 */
	public $name;

	/**
	 * A descriptive word or phrase for the low end of the spectrum for the descriptor.
	 *
	 * @var string
	 */
	public $low;

	/**
	 * A description of the low end of the spectrum for the descriptor.
	 *
	 * @var string
	 */
	public $high;

	/**
	 * An explanation of the descriptor.
	 *
	 * @var string
	 */
	public $description;

	/**
	 * A list of beers rated on the scale of the descriptor. From none of the descriptor to a lot of the descriptor.
	 *
	 * @var array(string)
	 */
	public $scale = array();

	public static function getAttributes($country, $lang, $descriptor_type)
	{
		$descriptors = array();

		$ci = get_instance();

		$ci->load->database();

		$q = "
		SELECT `index`,`t_descriptor`.`descriptor_id`, `property_type`, `value`
		FROM `t_descriptor`
		INNER JOIN `t_descriptor_{$country}` ON `t_descriptor`.`descriptor_id` = `t_descriptor_{$country}`.`descriptor_id`
		INNER JOIN `t_descriptor_property` ON `t_descriptor_{$country}`.`descriptor_id` = `t_descriptor_property`.`descriptor_id`
		INNER JOIN `t_content_field_{$lang}` ON `t_descriptor_property`.`lang_id` = `t_content_field_{$lang}`.`field_number`
		WHERE `t_descriptor_{$country}`.`type` = '{$descriptor_type}'
		ORDER BY `t_descriptor_{$country}`.`index` ASC";

		$query = $ci->db->query($q);

		$query_result = $query->result();

		foreach($query_result as $row)
		{
			if(!isset($descriptors[$row->index]))
			{
				$descriptors[$row->index] = new beer_descriptor();
				$descriptors[$row->index]->id = $row->descriptor_id;
			}

			$property_type = $row->property_type;

			$descriptors[$row->index]->$property_type = $row->value;
		}

		$q = "
		SELECT `t_descriptor_{$country}`.`index` AS descriptor_index, `t_descriptor_scale_{$country}`.`index` AS scale_index,`value`
		FROM `t_descriptor_scale_{$country}` INNER JOIN t_descriptor_{$country} ON `t_descriptor_{$country}`.`descriptor_id` = `t_descriptor_scale_{$country}`.`descriptor_id`
		ORDER BY `t_descriptor_scale_{$country}`.`descriptor_id`, `t_descriptor_scale_{$country}`.`index`";

		$query = $ci->db->query($q);

		$query_result = $query->result();

		foreach($query_result as $row)
		{
			$descriptors[$row->descriptor_index]->scale[$row->scale_index] = $row->value;
		}

		return $descriptors;
	}



}
?>