<?php
/**
 * .
 *
 * PHP Version 5.4
 *
 * @category PHP
 * @package
 * @subpackage None
 * @author Josh
 * @copyright 2012-2014 Be Agile, LLC
 * @license http://beagile.biz Contractual License Program
 * @link http://beagile.biz
 */

class attribute_list extends CI_Model
{
	/**
	 * A list of default quantitative attributes that describe the beer.
	 *
	 * @var array(BeerDescriptor)
	 */
	public $default = array();

	/**
	 * A list of advanced quantitative attributes that describe the beer.
	 *
	 * @var array(BeerDescriptor)
	 */
	public $advanced = array();

	public function setup($country, $lang)
	{
		$this->load->model('beer_descriptor');
		$this->default  = $this->getDefaultAttributes($country, $lang);
		$this->advanced = $this->getAdvancedAttributes($country, $lang);
	}

	public function getDefaultDescriptors($country, $lang)
	{
		$descriptors = beer_descriptor::getAttributes($country, $lang, 'default');

		return $descriptors;
	}

	public function getAdvancedDescriptors($country, $lang)
	{
		$descriptors = beer_descriptor::getAttributes($country, $lang, 'advanced');

		return $descriptors;
	}

}
?>