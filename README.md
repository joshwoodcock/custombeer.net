# README #

### Download Eclipse PDT: ### 

   https://eclipse.org/pdt/#download

### Install Apache Mysql PHP ###

Mac:

   a: Yosemite: http://jason.pureconcepts.net/2014/11/install-apache-php-mysql-mac-os-x-yosemite/

   b: Mavericks: https://guynathan.com/install-lamp-stack-on-mavericks-with-homebrew-with-php-mcrypt

Windows: 
 
   a: Windows 8: https://www.youtube.com/watch?v=Fxo4_DX4ejU

### Create a new project in EclipsePDT called "customebeer.net" ###
   

### What is this repository for? ###

Store the source code for CustomBeer.net

### How do I get set up? ###

MAC: 

git clone https://bitbucket.org/joshwoodcock/custombeer.net.git

Windows: 
TODO

Create a new project in EclipsePDT
File->New PHP Project
     :Project name: "custombeer.net"
     :Create a project from existing location



### Contribution guidelines ###

Just push your commits. If you do anything destructive it will be reverted. 

### Who do I talk to? ###

Any questions please contact joshwoodcock@beagile.biz