requirejs([
           'jquery',
           'models/BeerDescription',
           'models/DescriptionList',
           'models/AttributeDefinition',
           'models/Account',
           'views/ScalePanel',
           'views/BeerList',
           'views/Order',
           'views/BeerPoints',
           'views/More',
           'views/Menu',
           'purl'
], 
function($,BeerDescription,DescriptionList,AttributeDefinition,Account,ScalePanel,BeerList,Order,BeerPoints,More,Menu,purl){
	
	// Backbone options
	Backbone.emulateHTTP = true;

	// Models.
	var myAccount = new Account();

	// Collections.

	// Views.
	new BeerPoints({model:myAccount});
	new More({model:myAccount});
	new Menu({model:myAccount});

	// Controllers.
	Backbone.history.start();
	
	// Get language.
	var lang = $.url().param('lang');
	if(lang == undefined){lang='';}
	
	var descriptionList     = new DescriptionList({"lang":lang,page:'front-page'});
	var order               = new Order({model:descriptionList});
	var scalePanel          = new ScalePanel({el:'#sliders', model: descriptionList});
	
	// Load beers into list
	var beerList    = new BeerList({el:'#beer-list', model: descriptionList});
	

});

