define(['backbone'],function(Backbone){
	
	var AttributeDefinition = Backbone.Model.extend({
			
		defaults:{
			scales:[],
			highs:[],
			lows:[],
			descriptions:[],
			names:[],
			ids:[],
		},
		addDefinition:function(desc)
		{
			this.get('ids').push(desc.id);
			this.setName(desc.id,desc.name);
			this.setLow(desc.id,desc.low);
			this.setHigh(desc.id,desc.high);
			this.setDescription(desc.id,desc.description);
			this.setScale(desc.id,desc.scale);
			
		},
		getScale:function(id)
		{
			return this.get('scales')[id];
		},
		getHigh:function(id){
			return this.get('highs')[id];
		},
		getLow:function(id){
			return this.get('lows')[id];
		},
		getDescription:function(id){
			return this.get('descriptions')[id];
		},
		getName:function(id){
			return this.get('names')[id];
		},
		setScale:function(id,scale)
		{
			if(id != undefined && id != "" && id != null && scale != undefined && scale != null)
			{
				var scales = this.get('scales');
				scales[id] = scale;
				this.set('scales',scales);
			}
		},
		setHigh:function(id,high){
			if(id != undefined && id != "" && id != null && high != undefined && high != null)
			{
				var highs = this.get('highs');
				highs[id] = high;
				this.set('highs',highs);
			}
		},
		setLow:function(id,low){
			if(id != undefined && id != "" && id != null && low != undefined && low != null)
			{
				var lows = this.get('lows');
				lows[id] = low;
				this.set('lows',lows);
			}
		},
		setDescription:function(id,description){
			if(id != undefined && id != "" && id != null && description != undefined && description != null)
			{
				var descriptions = this.get('descriptions');
				descriptions[id] = description;
				this.set('descriptions',descriptions);
			}
		},
		setName:function(id,name){
			if(id != undefined && id != "" && id != null && name != undefined && name != null)
			{
				var names = this.get('names');
				names[id] = name;
				this.set('names',names);
			}
		},
		buildDefinition:function(descriptions){
			var default_desc = descriptions.default;
			for(i=0,k=default_desc.length;i<k;i++)
			{
				var description = default_desc[i];

				this.addDefinition(description);
			}
		}
	});
	
	return AttributeDefinition;
});