define(['backbone','models/BeerAttribute'],function(Backbone, BeerAttribute){
	
	var BeerDescription = Backbone.Model.extend({
		
		initialize:function(){
			
			this.bind("change:submitted",function(){
				if(this.get('submitted') == true){
					var id = this.get('id');
					
					$('beer_option_'+id).remove();
				}
			});
		},
		defaults:{
			attributes:[],
			img:"",
			name:"",
			id:"",
			submitted:false,
			description_id:undefined
		},
		makeReady : function(){
			var attributes = this.get('attributes');
			attributes.forEach(function(attr){
				attr.set('ready',true);
			});
		},
		makeCookieReady:function(attrDef)
		{
			var attributes = this.get('attributes');
			attributes.forEach(function(attr){
				
				if(attr.get('updated')==true)
				{
					attr.set('ready',true);
					//attr.updateValue(attr.get('value'),attrDef)
				}
			});
		},
		setupAttributes : function(data, show_examples){
			
			var attributes = [];
			
			for(j=0,h=data.attribute_values.length;j<h;j++)
			{
				var value = data.attribute_values[j].val;
				var id    = data.attribute_values[j].id;
				var beerAttribute = new BeerAttribute();
				
				beerAttribute.setup(value,id,show_examples);
				
				attributes[id] = beerAttribute;
			}

			this.set("img",data.img);
			this.set("name",data.name);
			this.set("attributes", attributes);
			this.set('submitted',data.submitted);
		},
		setupAttributeValues:function(data){
			
			this.set('description_id',data.di);
			
			if(data.s == '1')
			{
				this.set('submitted',true);
			}else{
				this.set('submitted',false);
			}
			
			var attributes = [];
			
			for(j=0,h=data.a.length;j<h;j++)
			{
				// get the attribute
				var id    = data.a[j].i;
				
				var beerAttribute = this.getAttribute(id);
				
				// get the value
				var value = data.a[j].v;
				var updated = (data.a[j].u == '1');
				beerAttribute.set('value',value);
				beerAttribute.set('updated',updated);
			}

		},
		getAttribute : function(id){
			return this.get('attributes')[id];
		},
		getScale : function(id){
			return this.get('scales')[id];
		},
		refreshAttributes:function(){
			
			var attributes = this.get('attributes');
			attributes.forEach(function(attr){
				var id = attr.get('id');
				$("#descriptor_id_"+id).val(attr.get('value'));
			});
			
			if(this.get('submitted') == true)
			{
				// Disable the sliders.
				$('.noUiSlider').attr('disabled','disabled');
			}else{
				// Enable the sliders.
				$('.noUiSlider').removeAttr('disabled');
			}
		},
		toJSON:function(){
			var attributes = this.get('attributes');
			var attribute_list = [];
			var desc_id    = this.get('description_id');
			attributes.forEach(function(attr){
				var id    = attr.get('id');
				var value = attr.get('value');
				var updated = (attr.get('updated') == true ? '1' : '0');
			
				var attribute = {
						"i":id,
						"v":value,
						"u":updated
				};
				attribute_list.push(attribute);
			});
			
			var submitted = (this.get('submitted') == true ? '1' : '0');
			description = {
				id:this.get('id'),
				a:attribute_list,
				s:submitted,
				di:desc_id
			}
			return description;
		}
	});
	
	return BeerDescription;
});

