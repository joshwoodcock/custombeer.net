define(['backbone','util'],function(Backbone,util){
	
	var Login = Backbone.Model.extend({
		
		url: site + '/account/login',
		
		defaults:{
			email:'',
			password:'',
			success:true
		},
		validate:function(attrs, options) {
						
			if(isEmail(attrs.email) == false)
			{
				var message = 'The email you provided is invalid';
				options.invalid(this, {name:'email', message:message});
				return message;
			}
			
			if(attrs.password == '')
			{
				var message = 'Please enter a password';
				options.invalid(this, {name:'password', message:message});
				return message;
			}
			
			if(attrs.success != true)
			{
				var message = attrs.message;
				options.invalid(this, {name:attrs.name, message:message});
				return message;
			}
		}
	});
	
	return Login;
});