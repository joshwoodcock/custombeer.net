define(['backbone','models/BeerDescription','models/AttributeDefinition','cookie'],function(Backbone,BeerDescription,AttributeDefinition,cookie){
	
	var DescriptionList = Backbone.Model.extend({

		initialize:function(){
			this.set('attribute_definition', new AttributeDefinition());
			
			var me = this;
			// Load beer description then run page functions
			$.get(site + '/main/beer_description?lang=' + this.get('lang'), function(beer_descriptions){
				me.get('attribute_definition').buildDefinition(beer_descriptions);
				
				if(me.get('get_beer_list') == true)
				{
					$.get(site + '/rate_a_beer/get_beer_list?level=1', function(beer_list){
						me.setup(false,beer_list);				
					});
				}else{
					me.setup(true)
				}
			});
		},
		defaults:{
			current_beer_id:null,
			attribute_definition:null,
			id_list:[],
			page:null,
			lang:'en',
			setup_complete:false,
			get_beer_list:false,
			beer_list:null,
			refreshing_beer:false
		},
		updateBeerList:function(){
			this.set('refreshing_beer', true);
			var me = this;
			$.get(site + '/rate_a_beer/get_beer_list?level=1', function(beer_list){
				me.setupFromList(false, beer_list);
				me.set('refreshing_beer', false);
			});
		},
		sendInterviews:function(){
			var id_list         = this.get('id_list');
			var descriptions    = [];
			
			for(i = 0,k = id_list.length;i < k; i++)
			{
				var description = this.get(id_list[i]);
				
				if(description.get('submitted') == true)
				{
					var json_desc   = JSON.stringify(description);
					var data        = {
							description:json_desc,
							country:'us'
					};
					
					$.post('./rate_a_beer/save_rate',data,function(response){
						var result = response.result;
						
						if(result != 'success')
						{
							console.log(response.message);
						}
					})
					.fail(function(response){
						console.log(response.message);
					});
				}
			}
		},
		setup:function(show_examples,beer_list){
			
			if(beer_list != undefined & beer_list != null){
				this.setupFromList(show_examples,beer_list);
				this.set('beer_list', beer_list);
			}else{
				this.setupFromDefault(show_examples);
			}
			
			// Load the cookie stored values if they exist.
			var desc_list = this.fetch();
			if(desc_list != undefined)
			{
				var id = desc_list.cbi;
				if(this.get(id) != undefined)
				{
					this.set('current_beer_id', id);
				}
				for(i=0,k=desc_list.descriptions.length;i<k;i++)
				{
					var desc_saved   = desc_list.descriptions[i];
					var desc_id      = desc_saved.id;
					var desc_current = this.get(desc_id);
					
					if(desc_current != undefined && desc_current.get('submitted') == false)
					{
						desc_current.setupAttributeValues(desc_saved);
					}
				}
			}
	
			this.set('setup_complete',true);
			this.set('refreshing', false);
		},
		setupFromList:function(show_examples,beer_list){
			
			var attributeDefinition = this.get('attribute_definition');
			
			//Initialize description objects
			for(i=0,k=beer_list.length;i<k;i++)
			{
				var beer = beer_list[i];
				var beerDescription = new BeerDescription({"id":beer_list[i].id});
				beerDescription.setupAttributes(beer,show_examples);
				this.addDescription(beerDescription);
			}
			
			// Set the current model
			this.set('current_beer_id',beer_list[0].id);
		},
		setupFromDefault:function(show_examples){
			var beerDescription = new BeerDescription({id:0});
			var attribute_values = [];
			var id_list          = this.get('attribute_definition').get('ids');
			for(i=0,k=id_list.length;i<k;i++)
			{
				attribute_values.push({id:id_list[i],val:3});
			}
			var beer = {
				attribute_values:attribute_values,
				id:0,
				img:"",
				name:"order",
				submitted:false
			};
			beerDescription.setupAttributes(beer, show_examples);
			this.addDescription(beerDescription);
			
			// Set the current model
			this.set('current_beer_id',0);
		},
		getCurrentDescription:function(){
			return this.get(this.get('current_beer_id'));
		},
		addDescription:function(description){
			var id = description.get('id');
			this.set(id, description);
			this.get('id_list').push(id);
		},
		makeReady:function(){
			var idList = this.get('id_list');
			for(a=0,b=idList.length;a<b;a++)
			{
				var description = this.get(idList[a]);
				description.makeReady();
			}
		},
		save:function(){
			var simpleMe = $.parseJSON(JSON.stringify(this));
			var page     = this.get('page');
			var idList   = [];
			
			for(i=0, k=simpleMe.descriptions.length; i<k;i++)
			{
				var desc    = simpleMe.descriptions[i];
				var desc_id = desc.id;
				idList.push(desc_id);
				
				$.cookie("description_list_"+page+"_id_" + desc_id,JSON.stringify(desc));
			}
			
			delete simpleMe.descriptions;
			simpleMe['idList'] = idList;
			
			$.cookie("description_list_"+page,JSON.stringify(simpleMe));
			var cookieMe = $.cookie("description_list_"+page);
		},
		fetch:function(){
			var page     = this.get('page');
			var desc_list_json = $.cookie("description_list_"+page);
			var descriptions = [];
			
			if(desc_list_json != undefined)
			{
				var desc_list = $.parseJSON(desc_list_json);
				
				for(i=0,k=desc_list.idList.length;i<k;i++)
				{
					var desc_id = desc_list.idList[i];
					var desc_json = $.cookie("description_list_"+page+"_id_"+desc_id);
					
					if(desc_json != undefined)
					{
						var desc = $.parseJSON(desc_json);
						descriptions.push(desc);
					}
				}
				
				desc_list['descriptions'] = descriptions;
				delete desc_list.idList;
			}
			
			return desc_list;
		},
		toJSON:function(){
			var current_beer_id = this.get('current_beer_id');
			var id_list         = this.get('id_list');
			var descriptions    = [];
			
			for(i = 0,k = id_list.length;i < k; i++)
			{
				descriptions.push(this.get(id_list[i]));
			}
			var desc_list = {
					cbi:current_beer_id,
					descriptions:descriptions
			};
			
			return desc_list;
		},
		unset:function(){
			$.removeCookie("description_list_front-page");
			$.removeCookie("description_list_rate-a-beer");
		}
	});
	
	return DescriptionList;
});