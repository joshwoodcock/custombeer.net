define(['backbone','models/Login','models/DescriptionList','util','purl'],function(Backbone,Login,DescriptionList,util,purl){
	
	var Account = Backbone.Model.extend({
		
		initialize:function(){
			this.siteGA = [];
			this.siteGA['qa.custombeer.net'] = 'UA-48286405-2';
			this.siteGA['custombeer.net'] = 'UA-48286405-1';
			this.updateLogin();
			

		},
		defaults:{
			is_logged_in:false,
			user_id:undefined,
			hook:null,
			login: new Login()
		},
		isLoggedIn:function(){
			return this.get('is_logged_in');
		},
		updateLogin:function(){
			var logged_in = this.retrieveLoggedIn();
			this.set('user_id',$.cookie('user_id'));
			this.set('is_logged_in',logged_in);
			if(logged_in == true)
			{
				this.login();
			}else{
				this.logout();
				this.gaPushUserId();
			}
		},
		gaSetupAccount:function(){
			
		},
		gaPushUserId:function(){
			// Try to send google analytics info. Might not exist in dev environments.
			if(typeof _gaq != 'undefined')
			{
				var user_id = this.get('user_id');
				console.log('_gaq is loaded.');
				console.log('user id: ' + user_id);
				var host = $.url().attr('host');
				var ga_account = this.siteGA[host];
				_gaq.push(['_setAccount', ga_account]);
				_gaq.push(['_setCustomVar',1,'User ID', user_id]);
				_gaq.push(['_trackPageview']);
			}
		},
		retrieveLoggedIn:function(){
			var logged_in = $.cookie('is_logged_in');
			if(logged_in == "true")
			{
				return true;
			}else{
				return false;
			}
		},
		login:function(){
			$.cookie('is_logged_in',true);
			this.set('is_logged_in',true);
			$.cookie('user_id', this.get('user_id'));

			this.gaPushUserId();
		},
		logout:function(){
			$.removeCookie('is_logged_in');

			this.set('is_logged_in',false);
			$('.login, .signup, .signup-login').removeClass('invisible');
			$('.logout').addClass('invisible');
		},
		loginSubmit:function(email, password, error, invalid){
			
			var login_data = {
					email:email,
					password:password,
					country:'us'
			};
			
			me = this;
			var success = function(model, response){
				me.set('user_id', response.user_id);
				me.login();
			};
			
			var options  = {
					success: success,
				    error: error,
				    invalid:invalid
			};
						
			this.get('login').save(login_data, options);
		},
		signupSubmit:function(){
			
			// Validate email.
			var email = $("#signup-email").val();
			if(isEmail(email) == false)
			{
				// Show error message.
				$("#signup-error").html("The email you provided is invalid");
				
				// Clear the error message when the value of the text box changes.
				$("#signup-email").on('input',function(){
					$("#signup-error").html("");
				});
				return;
			}
			
			// Validate passwords.
			var password = $("#signup-password").val();
			var password_confirm = $("#signup-password-confirm").val();
			if(password.length < 8)
			{
				// Show error message.
				$("#signup-error").html("Password must be at least 8 characters long");
				// Clear the error message when the value of the text box changes.
				$("#signup-password").on('input',function(){
					$("#signup-error").html("");
				});
				return;
			}
			if(password != password_confirm)
			{
				// Show error message.
				$("#signup-error").html("The passwords do not match");
				
				// Clear the error message when the value of either password text box changes.
				$("#signup-password").on('input',function(){
					$("#signup-error").html("");
				});
				$("#signup-password-confirm").on('input',function(){
					$("#signup-error").html("");
				});
				return;
			}
			
			var data = {
					email:email,
					password:password,
					country:'us'
			};
			
			var account = this;
			$.post('./account/signup',data,function(response){
				
				if(response.result == "success")
				{
					$("#signup-content div").html("Your account was successfully created. Welcome to CustomBeer.net!");
					account.set('user_id', response.user_id);
					account.login();
					$("#logged-in").click();
					if(account.get('hook') == 'order')
					{
						$(".order-button").click();
						account.set('hook',undefined);
					}
				}else{
					$("#signup-error").html(response.message);
					$("#signup-email").on('input',function(){
						$("#signup-error").html("");
					});
				}
			})
			.fail(function(response){
				$("#signup-error").html(errorMessage());
			});
			
		},
		logoutSubmit:function(success, error){
			var me = this;	
			$.post(site + '/account/logout',{},function(response){
				if(response.result == "success")
				{
					me.logout();
					if(success != undefined)
					{
						success(response);
					}
				}else{
					if(error != undefined)
					{
						error(response);
					}
				}
			})
			.fail(function(response){
				// logout error
			});
		},
		forgotPasswordSubmit:function(error_id){
			var account = this;
			// Validate email.
			var email = $("#forgot-password-email").val();
			if(isEmail(email) == false)
			{
				// Show error message.
				$("#forgot-password-error").html("The email you provided is invalid");
				
				// Clear the error message when the value of the text box changes.
				$("#forgot-password-email").on('input',function(){
					$("#forgot-password-error").html("");
				});
				return;
			}
			
			$(error_id).html('<span class="loader"><img src="img/ajax-loader-bar.gif"/></span>');
			
			$.post('./account/forgot_password',{email:email},function(response){
				
				
				
				
				if(response.result == "success")
				{
					$(error_id).html(response.message);
					$(error_id).addClass('success');
					$(error_id).removeClass('error');
				}else{
					if(response.error_type != undefined)
					{
						$(error_id).html(errorMessage());
					}else{
						$(error_id).html(response.message);
					}
					$(error_id).addClass('error');
					$(error_id).removeClass('success');
				}
			})
			.fail(function(response){
				// logout error
			});
		}
	});
	
	return Account;
});