define(['backbone'],function(Backbone){
	
	var BeerAttribute = Backbone.Model.extend({
			
		defaults:{
			id : null,
			value: null,
			ready: false,
			show_value_example:true,
			updated:false
		},
		
		setup:function(value,id,show_examples){
			this.set("value",value);
			this.set("id",id);
			this.set("show_value_example",show_examples);
		},
		
		updateValue:function(value, attributeDefinition,show_changed_examples){
			
			if(this.get('ready') == true)
			{
				this.set("value",value);
			}
			
			var id = this.get('id');
			if((this.get('ready') == true && this.get('show_value_example') == true) ||
					show_changed_examples == true && this.get('updated') == true)
			{
				var beer_value_example = attributeDefinition.getScale(id)[value];
				$("#descriptor_scale_" + id).html(value +" &#8776; "+beer_value_example);
				this.set('updated',true);
			}else{
				$("#descriptor_scale_" + id).html(value);
			}
		},
	});
	
	return BeerAttribute;
});