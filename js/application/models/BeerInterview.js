define(['backbone','jquery'],function(Backbone,$){
	
	/**
	 * Super view which can coordinate Account model and description list for the beer rate page.
	 */
	var BeerInterview = Backbone.Model.extend({
		
		initialize:function(){
			var account = this.get('account');
			var description_list = this.get('description_list');
			var me = this;
			
			account.on("change:is_logged_in", function(model, value){
				if(value == true)
				{
					me.updateScalePanel();
				}
			});
			
			description_list.on('change:refreshing_beer',function(model, value){
				if(value == false)
				{
					me.get('beer_list').update();
					me.set('refreshing_beer',false);
				}
			});
		},
		defaults:{
			account:null,
			description_list:null,
			beer_list:null,
			refreshing_beer:false
		},
		updateScalePanel:function(){
			var description_list = this.get('description_list');
			description_list.updateBeerList();
			this.set('refreshing_beer',true);
		}
		
	});
	
	return BeerInterview;

});