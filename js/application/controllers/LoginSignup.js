define(['backbone','jquery','simple_modal','util'],function(Backbone,$,SimpleModal,util){
	
	var Router = Backbone.Router.extend({
		
		routes:{
			"login":"login",
			"signup":"signup"
		}
		
	});
	
	return Router;
});