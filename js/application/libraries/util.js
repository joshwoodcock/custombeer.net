isEmail=function(email){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

errorMessage=function(){
	return '<div class="error">An error occured. Most of this code was written while drinking beer. Please excuse the errors.</div>';
}

removeAllCookies=function(){
	var cookies = document.cookie.split(";");
	for (var i = 0; i < cookies.length; i++){
		$.removeCookie(cookies[i].split("=")[0]);
	}
}