requirejs.config({
	
	urlArgs: ((typeof cb != 'undefined'  && cb == true) ? 'cb=' + Math.random(): ''),
	baseUrl: site + '/js/application/libraries',
	paths :{
		jquery:          'jquery-2.1.0.min',
		purl:            'purl-2.3.1.min',
		qtip:            'jquery.qtip-2.2.0.min',
		nouislider:      'jquery.nouislider',
		underscore:      'underscore',
		backbone:        'backbone',
		cookie:          'jquery.cookie-1.4.0.min',
		simple_modal:    'jquery.simplemodal-1.4.4.min',
		util:            'util',
		models:          '../models',
		views:           '../views',
		controllers:     '../controllers',
		spec:            '../../../js-tests/spec',
		jasmine:         '../../../js-tests/lib/jasmine/jasmine',
		'jasmine-html':  '../../../js-tests/lib/jasmine/jasmine-html',
		'jasmine-jquery':'../../../js-tests/lib/jasmine-jquery-1.3.1',
		main:            '../main'
	},
	shim: {
		underscore: {
		      exports: "_"
		 },
		'backbone':{
			deps:['underscore','jquery'],
			exports: 'Backbone'
		},
		'purl'         :['jquery'],
		'qtip'         :['jquery'],
		'nouislider'   :['jquery'],
		'cookie'       :['jquery'],
		'simple_modal' :['jquery'],
		jasmine: {
			exports: 'jasmine'
		},
		'jasmine-html': {
			deps: ['jasmine'],
			exports: 'jasmine'
		},
		util:{
			deps:['cookie']
		},
		main:{
			deps:['jquery','models/Account','views/BeerPoints','views/More','views/EventListener','views/Menu']
		}
	}
});