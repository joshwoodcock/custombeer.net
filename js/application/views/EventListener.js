define(['backbone','jquery'],function(Backbone,$){
	
	var EventListener = Backbone.View.extend({
		
		initialize:function(){
			$("#logged-in").click({view:this},this.sendInterviews);
		},
		el: "#bg",
		sendInterviews:function(event){
			var descriptionList = event.data.view.model;
			descriptionList.sendInterviews();
			
			$.get('rate_a_beer/get_beer_list?level=1', function(beer_list){
				
				descriptionList.setup(false,beer_list);
				
				desc = descriptionList.getCurrentDescription();
				
				desc.refreshAttributes();
				
				// update button.
				if(desc.get('submitted') == true)
				{
					// Show the submitted message.
					$('#cta').html(this.submitMessage);
					
					// Disable the sliders.
					$('.noUiSlider').attr('disabled','disabled');
				}else{
					// Show the submitted message.
					$('#cta').html(this.button);
					
					// Enable the sliders.
					$('.noUiSlider').removeAttr('disabled');
				}
			});
		}
	});
	
	return EventListener;

});