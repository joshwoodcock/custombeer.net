define(['backbone','jquery','simple_modal'],function(Backbone,$,SimpleModal){
	
	var BeerPoints = Backbone.View.extend({
		
		initialize:function(){
			this.el = $("#beer-points").clone();
			
			this.loginContent = $("#login-content").clone();
			
			this.content = this.$el.find('#beer-points-content').clone();
	
			this.content.removeClass('invisible');	
		},
		el:"#beer-points",
		render:function(){
			$.modal($("#beer-points-content"));
		},
		events:{
			"click a.render":"render"
		}
	});
	
	return BeerPoints;

});