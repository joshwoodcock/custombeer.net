define(['backbone','jquery','qtip','nouislider'],function(Backbone,$,qtip,nouislider,BeerAttribute){
	
	var ScalePanel = Backbone.View.extend({
		
		
		initialize: function(){
			this.template = this.$el.children();
			this.$el.html('');
			var me = this;
			this.model.on("change:setup_complete", function(model, value){
				if(value == true)
				{
					me.render();
					
					// Kill loading gif
					$('.img-loader').remove();
					
					// Post message to parent (if there is one) about the height of the screen.
					parent.postMessage($(document.body).height() + 125,"*");
				}
			});
		},
		render : function() {
			
			// Show the slider scales
			this.renderScales();
					
			// run NoUiSlider
			this.noUiSlider(true);
			
			// update description model
			this.model.getCurrentDescription().refreshAttributes();
			this.model.makeReady();
		},
		renderScales:function(){
			var def   = this.model.get('attribute_definition');
			var list  = def.get('ids');
			for(i=0,k=list.length;i<k;i++)
			{
				var id        = list[i];
				var name      = def.getName(id);
				var low       = def.getLow(id);
				var high      = def.getHigh(id);
				var desc_text = def.getDescription(id);
				var desc_html = this.template.clone();
				
				desc_html.find('.loader-img').remove();
				desc_html.attr('style','');
				desc_html.find('.sliderName').prepend(name);
				desc_html.find('.tip-container').attr('id','tip_container_'+i);
				desc_html.find('.info-img').attr('id','img-descriptor'+i);
				desc_html.find('.descriptor-value', this).attr('id','descriptor_scale_'+id);
				desc_html.find('.noUiSlider').attr('id', 'descriptor_id_'+id);
				
				// If the description has been submitted disable the sliders.
				desc_html.find('.noUiSlider').attr('disabled', 'disabled');

				desc_html.find('.sliderText').find('.sliderLow').attr('id','low-descriptor'+i).html(low);
				desc_html.find('.sliderText').find('.sliderHigh').attr('id','high-descriptor'+i).html(high);
			
				this.$el.append(desc_html);
		
				// Set tooltip content.
				var tool_tip = desc_text;
				this.toolTip(i, tool_tip);
			}
		},
		
		toolTip : function(index, toolTipContent){
			$('#img-descriptor'+index).qtip({
				content: toolTipContent,
				show:{
					event:'click',
					solo: true,
				},
				hide:{
					event:'unfocus'
				},
				position: {
					my: 'bottom center', 
			        at: 'top center',
					target:$('#tip_container_'+index)
			    },
			    style: {
			    	classes:'qtip-jtools info-tip',
			    }
			});
		},
		noUiSlider : function(show_changed_examples){
			
			var model = this.model;
			// Run noUiSlider
			$(".noUiSlider").noUiSlider({
				range: [1, 5]
				,start: 3
				,handles: 1
				,behaviour: 'extend-tap'
				,serialization: {
					resolution: 1.0,
					to: [function(value){
						
						var description = model.getCurrentDescription();
						
						// Get descriptor id.
						var elementID    = $(this).attr('id');
						if(elementID == 'descriptor_id_undefined'){return;}
						var descriptorID = parseInt(elementID.replace('descriptor_id_',''));
						
						// Parse the value.
						value = parseInt(value);
						
						// Update beer description model attribute value.
						var attribute = description.getAttribute(descriptorID);

						attribute.updateValue(value,model.get('attribute_definition'),show_changed_examples);
						
						// Save the model to cookies
						model.save();
					}]
				}
			});
		}
	});
	return ScalePanel;

});