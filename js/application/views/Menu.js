define(['backbone','jquery','simple_modal','util'],function(Backbone,$,SimpleModal,util){
	
	var Menu = Backbone.View.extend({
		
		initialize:function(){
			this.model.on("change:is_logged_in", this.showLoggedIn);
			this.showLoggedIn(this.model, this.model.get('is_logged_in'));
		},
		el:"body",
		login:function(){
			$.modal.close();
			$.modal($("#login-content"));
			$("#login-error").html("");

			$(document).keypress(function(e){
				if(e.which == 13)
				{
					$(".login-button").click();
				}
			});
		},
		signup:function(){
			$.modal.close();
			$("#signup-content").modal({
				autoResize:true,
				maxHeight:'2000px'
			});
			$("#signup-error").html("");
			
			$(document).keypress(function(e){
				if(e.which == 13)
				{
					$(".signup-button").click();
				}
			});
		},
		forgotPassword:function(){
			$.modal.close();
			$.modal($("#forgot-password-content"));
			$("#forgot-password-error").html("");
			
			$(document).keypress(function(e){
				if(e.which == 13)
				{
					$(".forgot-password-submit").click();
				}
			});
		},
		order:function(){
			var logged_in = this.model.isLoggedIn();
			
			/*start remove requirement to login before order.
			if(logged_in == true)
			{
			*/
				$.modal.close();
				$.modal($("#order-content"));
				$("#order-message").html("");
				
				$(document).keypress(function(e){
					if(e.which == 13)
					{
						$(".submit-order").click();
					}
				});
				/*
			}else{
				// Temporarily change the on change event response for change:is_logged_in.
				// Then change it back.
				var me = this;
				this.model.on("change:is_logged_in", function(){
					me.order();
					me.showLoggedIn(me.model, me.model.get('is_logged_in'));
					this.model.on("change:is_logged_in", this.showLoggedIn);
				});
				this.login();
			}
			 end remove requirement to login before order*/
			
		},
		showLoggedIn:function(model, logged_in){
			
			if(logged_in == true)
			{
				$("#login-content div").html("You are logged in. Welcome to Custombeer.net!");
				$('.login, .signup, .signup-login').addClass('invisible');
				$('.logout').removeClass('invisible');
			}
		},
		loginSubmit:function(){

			var email    = $("#login-email").val();
			var password = $("#login-password").val();
			
			var invalid    = function (model, error) {
				
				// Show error message.
		    	$("#login-error").html(error.message);
		    	
		    	// Clear the error message when the value of the text box changes.
				$("#login-" + error.name).on('input', function(){
					$("#login-error").html("");
				});
			};
			var error      = function(model, error){
				$("#login-error").html(errorMessage());
			};
			
			this.model.loginSubmit(email, password, error, invalid);
			
		},
		signupSubmit:function(){
			this.model.signupSubmit();
		},
		logoutSubmit:function(){
			var me        = this;
			
			// Success & error functions
			var success = function(response){
				alert("You have been signed out");
				location.reload();
			}
			var error = function(response){
				$("#more-error").html(errorMessage());
			}
			
			if(window.confirm("Are you sure? Press OK to logout."))
			{
				this.model.logoutSubmit(success,error);
			}
		},
		forgotPasswordSubmit:function(){
			this.model.forgotPasswordSubmit("#forgot-password-error");
		},
		events:{
			"click a.login":"login",
			"click a.signup":"signup",
			"click a.login-button":"loginSubmit",
			"click a.signup-button":"signupSubmit",
			"click a.logout":"logoutSubmit",
			"click a.forgot-password":"forgotPassword",
			"click a.forgot-password-submit":"forgotPasswordSubmit",
			"click a.order-button":"order"
		}
	});
	
	return Menu;
});