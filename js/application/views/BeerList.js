define(['backbone','jquery'],function(Backbone,$){
	
	var BeerList = Backbone.View.extend({
		
		initialize: function(){
			this.template      = this.$el.children();
			this.button        = $('#cta a');
			this.submitMessage = $('#cta-submit-response').clone();
			this.submitMessage.attr('style','');
			
			var me = this;
			this.model.on("change:beer_list", function(model, value){
				me.render(value);
			});
			
			this.model.on("change:setup_complete", function(model, value){
				var beer_id = me.model.get('current_beer_id');
				if(value == true && beer_id != 0)
				{
					me.$el.val(beer_id);
					me.update();
				}
			});
		},
		render : function(beer_list) {
			
			// Clone the template option and set the first beer.
			var option_html = this.template.clone();
			var beer        = beer_list[0];
						
			// Set and push the first option.
			this.$el.html('');
					
			// Set all the other options.
			for(i=0, k=beer_list.length;i<k;i++)
			{
				var option_html = this.template.clone();
				var beer        = beer_list[i];
				
				option_html.find('option');
				option_html.attr('value',beer.id);
				option_html.attr('id','beer_option_'+beer.id)
				option_html.html(beer.name);
				
				this.$el.append(option_html);
			}
			
			var current_beer_id = this.model.get('current_beer_id');
			$('#beer_option_'+current_beer_id).attr('selected','selected');
			
			this.update();
		},
		update: function(){
			var beerId      = this.$el.val();
			var description = this.model.get(beerId);
			this.model.set('current_beer_id',beerId);
			var img         = description.get('img');
			
			// Change the image
			if(img != '')
			{
				$('.logo').attr('src',site + '/img/beer/'+img);
			}else{
				$('.logo').attr('src',site + '/img/img-not-available.jpg');
			}
			
			// update slider values
			description.refreshAttributes();
			
			// update button.
			if(description.get('submitted') == true)
			{
				// Show the submitted message.
				$('#cta').html(this.submitMessage);
			}else{
				// Show the submitted message.
				$('#cta').html(this.button);
			}
			
			// update cookie storage.
			//this.model.save();
		},
		events: {
			"change":"update",
		}
	});
	
	return BeerList;

});