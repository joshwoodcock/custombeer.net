define(['backbone','jquery'],function(Backbone,$){
	
	var Cta = Backbone.View.extend({
		
		initialize: function(){
			this.submitMessage = $('#cta-submit-response').clone();
			this.button        = $('#cta-a').clone();
			this.submitMessage.attr('style','');
			this.confirm       = false;
			this.postUrl       = './rate_a_beer/save_rate';
		},
		el: "#cta",
		submit: function(){
			var confirmed = true;
			if(this.confirm == true)
			{
				var confirm = window.confirm("Are you sure? This cannot be undone.");
				if(confirm == false)
				{
					confirmed = false;
				}
			}
			if(confirmed == true)
			{
				var description = this.model.getCurrentDescription();
				var button      = this.button;
				var json_desc   = JSON.stringify(description);
				var data        = {
						description:json_desc,
						country:'us'
				};
				var view        = this;
				
				$('#cta').html('<img src="./img/ajax-loader.gif"/>');
				
				showError=function(){
					$('#cta').html(errorMessage);
					$('#cta').append(button);
				}
				
				$.post(this.postUrl,data,function(response){
					var result = response.result;
					var desc_id = response.desc_id;
					
					if(result == 'success')
					{
						// Update the submitted value of the model.
						var desc = view.model.getCurrentDescription();
						desc.set('submitted',true);
						desc.set('description_id',desc_id);
						view.model.save();
						
						// Show the submitted message.
						$('#cta').html(view.submitMessage);
						
						// Disable the sliders.
						$('.noUiSlider').attr('disabled','disabled');
					}else{
						showError();
					}
				})
				.fail(function(response){
					showError();
				});
			}
		},
		events: {
			"click a":"submit",
			"change input.logged-in":"sendInterviews"
		},
		sendInterviews:function(){
			alert('hello');
		}
	});
	
	return Cta;

});