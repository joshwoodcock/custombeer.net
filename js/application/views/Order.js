define(['backbone','jquery'],function(Backbone,$){
	
	var Order = Backbone.View.extend({
		
		initialize: function(){
			this.submitMessage = $('#cta-submit-response').clone();
			this.submitMessage.attr('style','');
			this.postUrl = './order/submit';
		},
		el: "#order-content",
		submit: function(){
			var description = this.model.getCurrentDescription();
			var button      = this.button;
			var json_desc   = JSON.stringify(description);
			
			// Validate zip
			var zip         = $("#order-zip").val();
			if($.isNumeric(zip) == false)
			{
				// Show error message.
				$("#order-message").html("The zip code you provided is invalid");
				
				// Clear the error message when the value of the text box changes.
				$("#order-zip").on('input',function(){
					$("#order-message").html("");
				});
				return;
			}
			
			// Validate email
			var email = $("#order-email").val();
			if(isEmail(email) == false)
			{
				// Show error message.
				$("#order-message").html("The email you provided is invalid");
				
				// Clear the error message when the value of the text box changes.
				$("#order-email").on('input',function(){
					$("#order-message").html("");
				});
				return;
			}
			
			var data        = {
					description:json_desc,
					country:'us',
					zip:zip,
					email:email
			};
			var view        = this;
			
			
			$('#order-message').html('<span class="loader"><img src="img/ajax-loader-bar.gif"/></span>');
			
			showError=function(){
				$('#order-message').html(errorMessage());
				$('#order-message').append(button);
			}
			
			$.post(this.postUrl,data,function(response){
				var result = response.result;
				var desc_id = response.desc_id;
				
				if(result == 'success')
				{
					// Update the submitted value of the model.
					var desc = view.model.getCurrentDescription();
					desc.set('submitted',true);
					desc.set('description_id',desc_id);
					view.model.save();
					
					// Show the submitted message.
					$('#cta').html(view.submitMessage);
					$('#cta span').html(response.message);
					$('#order-message').html('');
					$('.simplemodal-close').click();
					$('.noUiSlider').attr('disabled','disabled');
				}else{
					$('#order-message').html(response.message);
				}
			})
			.fail(function(response){
				showError();
			});
		
		},
		events: {
			"click a.submit-order":"submit"
		}
	});
	
	return Order;

});