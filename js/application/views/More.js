define(['backbone','jquery'],function(Backbone,$){
	
	var More = Backbone.View.extend({
		
		initialize:function(){
			this.el = $("#more").clone();
			
			this.content = this.$el.find('#more-content');
	
			this.content.removeClass('invisible');	
		},
		el:"#more",
		render:function(){
			$('#more-content').modal();
		},
		events: {
			"click":"render"
		}
	});
	
	return More;

});