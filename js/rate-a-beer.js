requirejs([
           'jquery',
           'models/BeerDescription',
           'models/DescriptionList',
           'models/Account',
           'views/ScalePanel',
           'views/BeerList',
           'views/Cta','purl',
           'views/EventListener',
           'views/BeerPoints',
           'views/More',
           'views/Menu',
           'models/BeerInterview'
], 
function($,BeerDescription,DescriptionList,Account,ScalePanel,BeerList,Cta,purl,EventListener,BeerPoints,More,Menu,BeerInterview){
	
	// Backbone options
	Backbone.emulateHTTP = true;

	// Models.
	var myAccount = new Account();

	// Collections.

	// Views.
	new BeerPoints({model:myAccount});
	new More({model:myAccount});
	new Menu({model:myAccount});

	// Controllers.
	Backbone.history.start();
	
	// Get language.
	var lang = $.url().param('lang');
	if(lang == undefined){lang='';}
	
	var descriptionList     = new DescriptionList({lang:lang, page:'rate-a-beer', get_beer_list:true});
	var cta                 = new Cta({model:descriptionList});
	
	var event               = new EventListener({model:descriptionList});
	var scalePanel          = new ScalePanel({el:'#sliders', model: descriptionList});
	var beerList            = new BeerList({el:'#beer-list', model:descriptionList});
	var beerInterview       = new BeerInterview({account:myAccount, description_list:descriptionList, beer_list:beerList});
	
	
	//set use confirmation dialog
	cta.confirm = true;
	cta.postUrl = 'rate_a_beer/save_rate';
});